# Instalação da stack do prometheus

> Este repositório tem como base o [repositório da stack utilizada pela comunidade](https://github.com/prometheus-operator/kube-prometheus/).

## Ordem de instalação

### 1.) Instalar os CRDs relativos ao prometheus, presentes na pasta ```.\setup\```

```command line
kubectl create -f .\setup\
```

### 2.) Instalar os manifestos da stack, presentes na pasta ```.\manifests\```

```command line
kubectl create -f .\manifests\
```

### 3.) Acessar o Prometheus via port-forward e verificar se todos os agents estão ```up```

```command line
kubectl -n monitoring port-forward svc/prometheus-operated 9090
```

Acessar o Prometheus em [localhost:9090](http://localhost:9090/)

### 4.) Acessar o Grafana via port-forward e verificar se a Database está disponível para consulta

```command line
kubectl -n monitoring port-forward svc/grafana 3000
```

Acessar o grafana em [localhost:3000](http://localhost:3000/)
